export type PromiseWithState<R> = Promise<R> & {
  resolved: boolean;
  rejected: boolean;
  complete: boolean;
};

export default function seePromiseState<R>(p: Promise<R>): PromiseWithState<R> {
  const sp = Object.assign(p, {
    resolved: false,
    rejected: false,
    complete: false,
  });
  sp.then(
    () => (sp.complete = true) && (sp.resolved = true),
    () => (sp.complete = true) && (sp.rejected = true)
  );
  return sp;
}
