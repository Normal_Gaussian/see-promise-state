import seePromiseState from '.';
const sleep = (ms: number) => new Promise(p => setTimeout(p, ms));

describe('stateful-promise', () => {
  it('should correctly mark a never completing promise, without a delay', async () => {
    const indefinite = seePromiseState(new Promise(() => {}));

    expect(indefinite.complete).toBe(false);
    expect(indefinite.resolved).toBe(false);
    expect(indefinite.rejected).toBe(false);
  });

  it('should correctly mark a never completing promise, after a delay', async () => {
    const indefinite = seePromiseState(new Promise(() => {}));

    await sleep(0);

    expect(indefinite.complete).toBe(false);
    expect(indefinite.resolved).toBe(false);
    expect(indefinite.rejected).toBe(false);
  });
  
  it('should fail to mark an immediately resolving promise, without a delay', async () => {
    const immediateResolution = seePromiseState(Promise.resolve());

    expect(immediateResolution.complete).toBe(false);
    expect(immediateResolution.resolved).toBe(false);
    expect(immediateResolution.rejected).toBe(false);
  });

  it('should fail to mark an immediately rejecting promise, after a delay', async () => {
    const immediateRejection = seePromiseState(Promise.reject());

    expect(immediateRejection.complete).toBe(false);
    expect(immediateRejection.resolved).toBe(false);
    expect(immediateRejection.rejected).toBe(false);
  });

  it('should correctly mark an immediately resolving promise, after a delay', async () => {
    const immediateResolution = seePromiseState(Promise.resolve());

    await sleep(0);

    expect(immediateResolution.complete).toBe(true);
    expect(immediateResolution.resolved).toBe(true);
    expect(immediateResolution.rejected).toBe(false);
  });

  it('should correctly mark an immediately rejecting promise, after a delay', async () => {
    const immediateRejection = seePromiseState(Promise.reject());

    await sleep(0);

    expect(immediateRejection.complete).toBe(true);
    expect(immediateRejection.resolved).toBe(false);
    expect(immediateRejection.rejected).toBe(true);
  });
});