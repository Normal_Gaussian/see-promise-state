# See Promise State

Adds synchronous state inspection to the native promise

Provides a function of the form:
```ts
<R>(p: Promise<R>) => PromiseWithState<R>
```

The returned stateful promise is *the same object* as the provided promise, with the following additional properties:

* `#resolved : boolean`
* `#rejected : boolean`
* `#complete : boolean`

Either of the following work:
```ts
const a = new Promise(task);
seePromiseState(a);

const a = seePromiseState(new Promise(task));
```
Useful for testing without introducting a mock for Promise. As such it is minorly limited; see caveats

## Example

```ts
import seePromiseState from 'see-promise-state';

const indefinite = new Promise(() => {});
seePromiseState(indefinite);

await sleep(0);
assert(indefinite.complete === false);
assert(indefinite.resolved === false);
assert(indefinite.rejected === false);

const immediateResolution = Promise.resolve();
seePromiseState(immediateResolution);

await sleep(0);
assert(immediateResolution.complete === true);
assert(immediateResolution.resolved === true);
assert(immediateResolution.rejected === false);

const immediateRejection = Promise.reject();
seePromiseState(immediateRejection);

await sleep(0);
assert(immediateRejection.complete === true);
assert(immediateRejection.resolved === false);
assert(immediateRejection.rejected === true);
```

## Caveats

Does not immediately apply to immediately resolving promises:

```ts
seePromiseState(Promise.resolve()).complete
// false

x = seePromiseState(Promise.resolve())
await sleep(0);
x.complete
// true
```

May not be correct for chains started before conversion
```ts
x = asyncTask();
x.then(() => console.log('A: ' + x.complete));
seePromiseState(x);
x.then(() => console.log('B: ' + x.complete));
await x;
// > 'A: false'
// > 'B: true'
```

## Contributing

Feel free to make any contributions

The project uses yarn berry with zero-installs.

Build with: `yarn build`
Test with: `yarn test`

If you use vscode, running `yarn setupVSCode` will setup vscode's typescript with yarn pnp.

If you use vscode launch configs are included for testing.
