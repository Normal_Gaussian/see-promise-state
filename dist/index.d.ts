export declare type PromiseWithState<R> = Promise<R> & {
    resolved: boolean;
    rejected: boolean;
    complete: boolean;
};
export default function seePromiseState<R>(p: Promise<R>): PromiseWithState<R>;
