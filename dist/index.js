"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function seePromiseState(p) {
    const sp = Object.assign(p, {
        resolved: false,
        rejected: false,
        complete: false,
    });
    sp.then(() => (sp.complete = true) && (sp.resolved = true), () => (sp.complete = true) && (sp.rejected = true));
    return sp;
}
exports.default = seePromiseState;
//# sourceMappingURL=index.js.map